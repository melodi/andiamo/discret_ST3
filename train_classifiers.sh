#!/usr/bin/env bash

# IMPORTANT: Add the path to your cloned DISRPT repo!
# This script will train all three classifiers (and 2 adapters)
# needed for this task. 
# First script does not need the adapters, but the last 2 ones do.
# If you do not have the adapters, please run all five scripts. 
# If you have the adapters trained,
# you can directly run the classifiers.

# To reproduce the results of MELODI team, do NOT change other params.

# bare classifier
python pytorch_classifier.py --num_epochs 5 --data_path 'PATH_TO_REPO'

# Train the adapters:
python make_adapter.py --num_epochs 15 --data_path 'PATH_TO_REPO' --freeze_layers 'layer.1;layer.2;layer.3'
python make_adapter.py --num_epochs 15 --data_path 'PATH_TO_REPO' --freeze_layers 'layer.1'

# Run classifiers with adapters
python adapter_classifier.py --num_epochs 3 --data_path 'PATH_TO_REPO' --adapter_name 'adapter_15-epochs_frozen-1'
python adapter_classifier.py --num_epochs 4 --data_path 'PATH_TO_REPO' --adapter_name 'adapter_15-epochs_frozen-1-2-3'