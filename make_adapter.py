#!/usr/bin/env python
# coding: utf-8

import os
import numpy as np
from datetime import datetime
import pandas as pd
import torch
from transformers import AutoModel, AutoTokenizer, AutoModelWithHeads, AutoConfig, TrainingArguments, AdapterTrainer, EvalPrediction, set_seed
import datasets
from configure import parse_args
from sklearn.metrics import accuracy_score
from utils import *

# parameters
args = parse_args()
tokenizer = AutoTokenizer.from_pretrained(args.transformer_model)
layers_to_freeze = args.freeze_layers.split(';')
set_seed(42)
batch_size = args.batch_size
mapping_classes = args.mappings_file[:-4].split('-')[-1]

# Set name for adapter
adapter_name = 'A_' + str(args.num_epochs) + '-F_' + args.freeze_layers.replace('layer.', '-').replace(';', '') + '-M_' + mapping_classes

print('Create classifier adapter\n')
print('Name:', adapter_name)
print('Model:', args.transformer_model)
print('Batch size:', args.batch_size * args.gradient_accumulation_steps)
print('Frozen layers:',  args.freeze_layers.replace(';', ', '))

# Open mappings
mappings, inv_mappings = open_mappings(args.mappings_file)

# Open sentences
train_sentences, dev_dict_sentences, _ = open_sentences(args.data_path, mappings)

# make pandas dataframes
file_header = ['text', 'labels']
train_df = pd.DataFrame([[' '.join(x[-2]), x[-1]] for x in train_sentences], columns=file_header)
train_df = train_df.sample(frac = 1) # shuffle the train
# get a global dev accuracy, we will not be directly using these results
dev_df = pd.DataFrame([[' '.join(x[-2]), x[-1]] 
                       for sents in dev_dict_sentences.values()
                       for x in sents ], columns=file_header)

#Make datasets from dataframes
train_dataset = datasets.Dataset.from_pandas(train_df)
dev_dataset = datasets.Dataset.from_pandas(dev_df)

# get number of labels
num_labels = len(set([int(x.strip()) 
                      for x in train_df['labels'].to_string(index=False).split('\n')])) +1

# Encode the data
train_dataset = train_dataset.map(encode_batch, batched=True)
train_dataset.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])

dev_dataset = dev_dataset.map(encode_batch, batched=True)
dev_dataset.set_format(type="torch", columns=["input_ids", "attention_mask", "labels"])


# Training

config = AutoConfig.from_pretrained(
    args.transformer_model,
    num_labels=num_labels,
)
model = AutoModelWithHeads.from_pretrained(
    args.transformer_model,
    config=config,
)

# Add a new adapter
model.add_adapter(adapter_name)
# Add a matching classification head
model.add_classification_head(
    adapter_name,
    num_labels=num_labels,
    id2label=inv_mappings
  )

# Activate the adapter
print('Initialize adapter...')
model.train_adapter(adapter_name)

training_args = TrainingArguments(
    learning_rate    = 1e-4,
    num_train_epochs = args.num_epochs,
    per_device_train_batch_size = args.batch_size,
    per_device_eval_batch_size  = args.batch_size,
    gradient_accumulation_steps = args.gradient_accumulation_steps,
    logging_steps  = (len(train_sentences)/(args.batch_size * args.gradient_accumulation_steps)),
    output_dir = "./training_output",
    overwrite_output_dir =True,
    remove_unused_columns=False,
)

trainer = AdapterTrainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
)

# freeze layers
if args.freeze_layers != '':
    for name, param in model.named_parameters():
        if any(x in name for x in layers_to_freeze):
            param.requires_grad = False

# Start the training 🚀
print('\nStart training...\n')
trainer.train()

# Save adapter to load for the finetuned model
model.save_adapter(adapter_name, adapter_name)

# Perform evaluation
results = trainer.predict(dev_dataset)
preds = np.argmax(results.predictions, axis=1)
results = results.label_ids
print('Dev accuracy:', round(accuracy_score(preds, results), 4))